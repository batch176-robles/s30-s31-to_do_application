//Controllers contains the functions and business logic of our Express JS Application

//Uses the "require" directive to allow access to the "Task" model which allows us to access mongoose methods to perform CRUD functions
//Allows us to use the contents of the "task.js" file in the "models" folder
const { findByIdAndUpdate } = require("../models/task");
const Task = require("../models/task");

//[SECTION] - CONTROLLERS 

//[SECTION] - Create
//The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {


    //Creates a task object based on the Mongoose model "Task"
    let newTask = new Task({

        //Sets the "name" property with the value received from the client/Postman
        name: requestBody.name
    })


    // Saves the newly created "newTask" object in the MongoDB database
    // The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
    // The "then" method will accept the following 2 arguments:
    // The first parameter will store the result returned by the Mongoose "save" method
    // The second parameter will store the "error" object
    // Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
    // Ex.
    // newUser.save((saveErr, savedTask) => {})
    return newTask.save().then((task, error) => {

        // If an error is encountered returns a "false" boolean back to the client/Postman
        if (error) {
            return false
            // Save successful, returns the new task object back to the client/Postman
        } else {
            return task
        }
    })


}
//[SECTION] - Retrieve
module.exports.getAllTasks = () => {

    //The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function the "/tasks" routes is accessed

    return Task.find({}).then(result => {

        //The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
        return result
    })
};

// [SECTION] Update
// 1. Change status of Task. pending -> 'completed'.
// we will reference the document using it's ID field.
module.exports.taskCompleted = (taskId) => {
    // Query/Search for the desired task to update.
    // The "findById" mongoose method will look for a resource (task) which matches the ID from the URL of the request.
    // upon performing this method inside our collection, a new promise will be instantiated, so we need to be able to handle the possible outcome of that promise.
    return Task.findById(taskId).then((found, error) => {
        // describe how were going to handle the outcome of the promise using a selection control structure.

        // error -> rejected state of the promise
        if (found) {
            // call out the parameter that describes the result
            console.log(found)
            found.status = 'Completed';
            // Save the new changes inside our database.
            // upon saving the new changes for this document a 2nd promise will be instantiated.
            // we will chain a thenable expression upon performing a save() method into our returned document.
            return found.save().then((taskUpdated, error) => {
                // catch the state of the promise to identify a specific response.
                if (taskUpdated) {
                    return 'Task has been successfully modified';
                } else {
                    // return the actual error
                    return 'Task failed to Update'
                }
            })

        } else {
            return 'Something Went Wrong';
        };
    });
};


module.exports.taskPending = (userInput) => {
    // expose this new component
    // search the database for the user Input
    return Task.findById(userInput).then((found, error) => {
        if (found) {
            found.status = 'Pending';
            return found.save().then((suc, err) => {
                if (suc) {
                    return `Task ${found.name} has been changed to Pending`
                } else {
                    return 'Task failed to revert'
                }
            })
        } else {
            return 'Error! No Document Found. Tsk!'
        }
    })
}


// [SECTION] Destroy
module.exports.deleteTask = (idNgTask) => {
    // how will the data will be processed in order to execute the task.
    // select which mongoose method will be used in order to acquire the desired end goal.
    // Mongoose -> it provides an interface and methods to be able to manipulate the resources inside the mongoDB atlas.

    // in order to identify the location in which the function will be executed append the model name.
    return Task.findByIdAndRemove(idNgTask).then((fulfilled, rejected) => {
        if (fulfilled) {
            return 'The Task has been successfully Removed';
        } else {
            return 'Failed to remove Task'
        }
    });
    // assign a new endpoint for this route.
};


let dave = []